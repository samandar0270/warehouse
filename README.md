Ishlab chiqarish korxonasida mahsulot ishlab chiqarish uchun kerak
bo’ladigan xomashyolar haqida (omborxonada yetarlicha xomashyo bormi,
qancha miqdorda bor, qancha yetmayapti, qaysi partiyadan qancha miqdorda
olinyapti) ma’lumot olish uchun omborxonaga so’rov yuborish. Mahsulot ishlab
chiqarish uchun ishlatilinadigan xomashyolar oldindan kiritilgan bo’ladi.