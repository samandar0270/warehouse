from django.contrib import admin
from .models import Product, ProductMaterial, Warehouse, RawMaterial


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']


@admin.register(ProductMaterial)
class ProductMaterialAdmin(admin.ModelAdmin):
    list_display = ['product', 'material', 'quantity']


@admin.register(Warehouse)
class WarehouseAdmin(admin.ModelAdmin):
    list_display = ['material', 'remainder', 'price']


@admin.register(RawMaterial)
class RawMaterialAdmin(admin.ModelAdmin):
    list_display = ['name',]
