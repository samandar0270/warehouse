from rest_framework import serializers
from .models import Product


class ProductQtySerializer(serializers.Serializer):
    product_name = serializers.CharField()
    qty = serializers.IntegerField()

    def validate_product_name(self, value):
        if not Product.objects.filter(name=value).exists():
            raise serializers.ValidationError(f"Product with '{value}' not found")
        return value


class MaterialInfoRequestSerializer(serializers.Serializer):
    products = ProductQtySerializer(many=True)
