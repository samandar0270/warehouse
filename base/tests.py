from rest_framework.test import APITestCase
from .models import Product, ProductMaterial, RawMaterial, Warehouse


class TestGetMaterialInfo(APITestCase):
    def test_product_not_found(self):
        response = self.client.post("/get_material_info", format="json", data={
            "products": [
                {"product_name": "Ko'ylak", "qty": 10}
            ]
        })
        self.assertEquals(response.status_code, 400)
        self.assertIn("Product with \'Ko\'ylak\' not found", response.data['products'][0]["product_name"])

    def test_return_only_requested_products(self):
        Product.objects.create(name="Ko'ylak", code="Ko'ylak")
        Product.objects.create(name="Shim", code="Shim")
        Product.objects.create(name="Shapka", code="Shapka")
        response = self.client.post("/get_material_info", format="json", data={
            "products": [
                {"product_name": "Ko'ylak", "qty": 10},
                {"product_name": "Shapka", "qty": 10}
            ]
        })
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data['result']), 2)
        self.assertEquals(response.data['result'][0]['product_name'], "Ko'ylak")
        self.assertEquals(response.data['result'][1]['product_name'], "Shapka")

    def test_product_materials(self):
        material_mato = RawMaterial.objects.create(name="Mato")
        material_zamok = RawMaterial.objects.create(name="Zamok")
        material_ip = RawMaterial.objects.create(name="Ip")
        material_tugma = RawMaterial.objects.create(name="Tugma")

        warehouse1 = Warehouse.objects.create(material=material_mato, remainder=12, price=1500)
        warehouse2 = Warehouse.objects.create(material=material_mato, remainder=200, price=1600)
        warehouse3 = Warehouse.objects.create(material=material_ip, remainder=40, price=500)
        warehouse4 = Warehouse.objects.create(material=material_ip, remainder=300, price=550)
        warehouse5 = Warehouse.objects.create(material=material_tugma, remainder=500, price=300)
        warehouse6 = Warehouse.objects.create(material=material_zamok, remainder=1000, price=2000)

        product1 = Product.objects.create(name="Ko'ylak", code="Ko'ylak")
        ProductMaterial.objects.create(product=product1, material=material_ip, quantity=10)
        ProductMaterial.objects.create(product=product1, material=material_tugma, quantity=5)
        ProductMaterial.objects.create(product=product1, material=material_mato, quantity=0.8)

        product2 = Product.objects.create(name="Shim", code="Shim")
        ProductMaterial.objects.create(product=product2, material=material_ip, quantity=15)
        ProductMaterial.objects.create(product=product2, material=material_zamok, quantity=1)
        ProductMaterial.objects.create(product=product2, material=material_mato, quantity=1.4)

        response = self.client.post("/get_material_info", format="json", data={
            "products": [
                {"product_name": "Ko'ylak", "qty": 30},
                {"product_name": "Shim", "qty": 20},
            ]
        })
        self.assertEquals(response.status_code, 200)
        expected_data = {
            "result": [
                {
                    "product_name": "Ko'ylak",
                    "qty": 30,
                    "product_materials": [
                        {
                            "warehouse_id": 3,
                            "material_name": "Ip",
                            "qty": 40.0,
                            "price": 500.0
                        },
                        {
                            "warehouse_id": 4,
                            "material_name": "Ip",
                            "qty": 260.0,
                            "price": 550.0
                        },
                        {
                            "warehouse_id": 5,
                            "material_name": "Tugma",
                            "qty": 150.0,
                            "price": 300.0
                        },
                        {
                            "warehouse_id": 1,
                            "material_name": "Mato",
                            "qty": 12.0,
                            "price": 1500.0
                        },
                        {
                            "warehouse_id": 2,
                            "material_name": "Mato",
                            "qty": 12.0,
                            "price": 1600.0
                        }
                    ]
                },
                {
                    "product_name": "Shim",
                    "qty": 20,
                    "product_materials": [
                        {
                            "warehouse_id": 4,
                            "material_name": "Ip",
                            "qty": 40.0,
                            "price": 550.0
                        },
                        {
                            "warehouse_id": None,
                            "material_name": "Ip",
                            "qty": 260.0,
                            "price": None
                        },
                        {
                            "warehouse_id": 6,
                            "material_name": "Zamok",
                            "qty": 20.0,
                            "price": 2000.0
                        },
                        {
                            "warehouse_id": 2,
                            "material_name": "Mato",
                            "qty": 28.0,
                            "price": 1600.0
                        }
                    ]
                }
            ]
        }
        self.assertEquals(response.json(), expected_data)
