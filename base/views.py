from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from .serializers import MaterialInfoRequestSerializer
from .models import Warehouse, Product, ProductMaterial


class MaterialInfoView(GenericAPIView):
    serializer_class = MaterialInfoRequestSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        warehouses = list(Warehouse.objects.all())

        result = []
        for product_info in serializer.data["products"]:
            data = {
                "product_name": product_info["product_name"],
                "qty": product_info["qty"],
                "product_materials": []
            }
            product = Product.objects.get(name=product_info['product_name'])
            for product_material in ProductMaterial.objects.filter(product=product):
                found_qty = 0
                for warehouse in warehouses:
                    if warehouse.material == product_material.material and warehouse.remainder > 0:
                        needed_material_qty = product_material.quantity * product_info["qty"] - found_qty

                        if needed_material_qty >= warehouse.remainder:
                            data["product_materials"].append(
                                {
                                    "warehouse_id": warehouse.id,
                                    "material_name": product_material.material.name,
                                    "qty": warehouse.remainder,
                                    "price": warehouse.price
                                }
                            )
                            found_qty += warehouse.remainder
                            warehouse.remainder = 0
                        else:
                            data["product_materials"].append(
                                {
                                    "warehouse_id": warehouse.id,
                                    "material_name": product_material.material.name,
                                    "qty": needed_material_qty,
                                    "price": warehouse.price
                                }
                            )
                            found_qty += needed_material_qty
                            warehouse.remainder -= needed_material_qty

                        if found_qty == product_material.quantity * product_info["qty"]:
                            break
                    elif warehouse.remainder == 0:
                        continue
                needed_material_qty = product_material.quantity * product_info["qty"]
                if needed_material_qty > found_qty:
                    data["product_materials"].append(
                        {
                            "warehouse_id": None,
                            "material_name": product_material.material.name,
                            "qty": needed_material_qty-found_qty,
                            "price": None
                        }
                    )
            result.append(data)

        return Response({"result": result})
