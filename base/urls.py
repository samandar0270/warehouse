from django.urls import path
from .views import MaterialInfoView

urlpatterns = [
    path('get_material_info', MaterialInfoView.as_view(), name='calculate_required_raw_materials'),
]
